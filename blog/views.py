from django.shortcuts import render
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status
from utils.models import Variables, VariableType, Artworks
from django.core import serializers
from utils.models import Images
from utils.models import ImageType
from utils.templatetags.file_exists import file_exists
from artist.settings import SITE_URL
# Create your views here.

class Home(APIView):

    def get(self, request, *args, **kwargs):
        variables = [ 'header_image', 'artwork_content', 'artist_image', 'artist_content', 'email_content']

        data = {'artworks': {}}
        variables = Variables.objects.filter(key__in=variables).all()
        artworks = Artworks.objects.order_by('-id').all()[0:3]

        for i in variables:
            value = ''
            if i.type == VariableType.image.value:
                image = Images.objects.filter(object_id=i.pk, type=ImageType.variable).order_by('rank').first()
                if str(file_exists(image.get_path(image))):
                    value = SITE_URL+image.get_path(image)
            else:
                value = i.value

            data[i.key] = value

        data['artworks'] = [{  'image': SITE_URL+k.getImagePath, 'name': k.name, 'id': k.id } for i, k in enumerate(artworks)]

        return Response(
            {'success': True, 'data': data, },
            status=status.HTTP_200_OK)


