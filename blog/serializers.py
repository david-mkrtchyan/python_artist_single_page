from users.models import Users as User
from django.utils.translation import gettext as _
from django.contrib.auth.hashers import make_password
from django_countries import countries
from utils.models import Accounts, Currencies, TransactionSend
from rest_framework import serializers
from rest_framework_jwt.settings import api_settings
from django.db import DatabaseError, transaction

jwt_payload_handler = api_settings.JWT_PAYLOAD_HANDLER
jwt_encode_handler = api_settings.JWT_ENCODE_HANDLER

