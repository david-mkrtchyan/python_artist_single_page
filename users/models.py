from django.db import models
from django.contrib.auth.models import AbstractUser
# Create your models here.


# Create your models here.
class Users(AbstractUser):
    # add another user columns

    class Meta:
        swappable = 'AUTH_USER_MODEL'
        db_table = 'auth_user'

    def __str__(self):
        return self.first_name + ' ' + self.last_name
