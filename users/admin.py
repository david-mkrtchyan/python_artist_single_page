from django.contrib import admin
from .models import Users
from django.utils.translation import gettext_lazy as _
from django.utils.safestring import mark_safe
from django.contrib.auth.admin import UserAdmin

# Register your models here.
class UserAdminCustom(UserAdmin):


    list_display = ('username', 'email', 'first_name', 'last_name', 'is_staff', )

    fieldsets = (
        (None, {'fields': ('username', 'password')}),
        (
            _('Personal info'), {'fields': (
                'first_name', 'last_name', 'email',
            )}),
        # (_('Additional info'),
        #  {'fields': ()}),
        (_('Permissions'), {'fields': ('is_active', 'is_staff', 'is_superuser',
                                       'groups', 'user_permissions')}),
        (_('Important dates'), {'fields': ('last_login', 'date_joined')}),
    )

    def save_model(self, request, obj, form, change):
        obj.save()

        super(UserAdminCustom, self).save_model(request, obj, form, change)

admin.site.register(Users, UserAdminCustom)