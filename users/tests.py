from django.test import TestCase
from django.contrib.auth.models import AbstractUser
# Create your tests here.

class Users(AbstractUser):
    list_display = ('email', 'first_name', 'last_name', 'phone_number', 'is_staff',)

    class Meta:
        swappable = 'AUTH_USER_MODEL'
        db_table = 'auth_user'

    def __str__(self):
        return self.first_name + ' ' + self.last_name

