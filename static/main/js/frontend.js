var front = jQuery.noConflict();

front(document).ready(function () {

    if (front('#language-list').length) {
        new Vue({
            el: '#language-list',
            methods: {
                set_lang: function (e) {
                    e.preventDefault()
                    let name = e.target.value;
                    let url = '/i18n/setlang/'
                    let csrftoken = front('input[name="csrfmiddlewaretoken"]').val();

                    const options = {
                        method: 'post',
                        params: {
                            language: name,
                            csrfmiddlewaretoken: csrftoken,
                        },
                        responseType: 'json',
                    };

                    front.post(url, {language: name, csrfmiddlewaretoken: csrftoken}, function (data) {
                        location.reload();
                    });

                }
            }
        });
    }

    if (front('#body-product')) {
        front('#body-product').showMore({
            minheight: 74,
            buttontxtmore: "Show more",
            buttontxtless: "Show less",

        });
    }

    front(document).on('change', '.brand-model', function (e) {
        e.preventDefault()
        let csrftoken = front('input[name="csrfmiddlewaretoken"]').val();

        front.post('/car/get-models/', {id: front(this).val(), csrfmiddlewaretoken: csrftoken}, function (data) {
            front('.model-import').find('option').remove()
            if (data.success == true) {
                front.each(data.models, function (k, v) {
                    front('.model-import').append('<option value="' + v[1] + '">' + v[0].name + '</option>')
                });

                var modelSelected = front('.model-import').attr('data-selected')
                if (modelSelected) {
                    front(".model-import option").each(function () {
                        if (front(this).val() == modelSelected) {
                            front(this).attr("selected", "selected");
                        }
                    });
                }

                changeTitleBrandModel()

            } else {
                front('.model-import').append('<option>' + gettext('Model') + '</option>')
            }

            setTimeout(function () {
                jQuery('.model-import').change()
            }, 4)

        });
    });

    front('.favourite').click(function (e) {
        e.preventDefault()

        var thisd = front(this)

        front.post('/add-favorite/', {
            id: front(this).data("slug"),
            csrfmiddlewaretoken: front(this).data("csrf")
        }, function (data) {
            if (data && data.success) {
                if (data.addFavorite == true) {
                    thisd.text('In Favorite List')
                } else {
                    thisd.text('Add to Favorites')
                }
                // if (data.favorite == true) {
                //     thisd.find('i').removeClass('fa-heart-o')
                //     thisd.find('i').addClass('fa-heart')
                //
                //     if (thisd.closest('.favourite-view').length) {
                //         thisd.find('.add-to-favorite').text(gettext('Remove from favorites'))
                //     }
                //
                // } else {
                //     if (thisd.closest('.favorites-lists').length) {
                //         thisd.closest('.favorite-item').remove()
                //     }
                //
                //     if (thisd.closest('.favourite-view').length) {
                //         thisd.find('.add-to-favorite').text(gettext('Add to favorites'))
                //     }
                //     thisd.find('i').addClass('fa-heart-o')
                //     thisd.find('i').removeClass('fa-heart')
                //
                // }
            }

        });
    })


    var finOpt = front('#finOpt')
    var finOpt_second = front('#finOpt_second')
    var call_back = front('#call_back-form')
    var ask_question = front('#ask_question-form')
    var followingform = front('#following-form')

    financing_request_info(finOpt)
    financing_request_info(finOpt_second)
    financing_request_info(call_back)
    financing_request_info(ask_question)
    financing_request_info(followingform, '/user/follow/')

    function financing_request_info(finOpt, path = "/form-req-financing/") {
        if (finOpt.length) {

            finOpt.validator().on('submit', function (e) {
                finOpt.find('.form-success').addClass('hide')
                var fronttarget = finOpt.find('.form-response');
                if (e.isDefaultPrevented()) {
                    fronttarget.html("");
                } else {
                    // Ajax call to load php file to process mail function
                    front.ajax({
                        url: path,
                        type: "POST",
                        data: finOpt.serialize(),
                        beforeSend: function () {
                            //fronttarget.html("<div class='alert alert-info'><p>Loading ...</p></div>");
                        },
                        success: function (text) {
                            if (text && text.success == true) {
                                if (text.redirect == true) {
                                    location.reload();
                                } else {
                                    finOpt.trigger("reset");
                                    finOpt.find('.form-success').removeClass('hide')
                                }
                            }
                        },
                        error: function (text) {
                            if (text && text.responseText) {
                                var obj = jQuery.parseJSON(text.responseText);
                                finOpt.find('.help-block.with-errors').html('');
                                front.each(obj, function (k, v) {
                                    var input = finOpt.find("input[name*='" + k + "']");
                                    var textarea = finOpt.find("textarea[name*='" + k + "']");
                                    var groupParent = ''

                                    if (input.length) {
                                        groupParent = input.closest('.form-group')
                                    } else if (textarea.length) {
                                        groupParent = textarea.closest('.form-group')
                                    }

                                    if (groupParent.length) {
                                        groupParent.addClass('has-error has-danger')
                                        groupParent.find('.with-errors').append('<ul class="list-unstyled"><li>' + v + '</li></ul>')
                                    }
                                });
                            }
                        }
                    });
                    return false;
                }
            });

        }
    }


    front('.model-import').change(function () {
        changeTitleBrandModel()
    })

    front('.flex_row_center').find('.cobalt-Stepper__ActionButton').click(function (e) {
        e.preventDefault()
        var th = front(this)
        var type = th.attr('data-type')
        var suggestButton = th.closest('.flex_row_center').find('.cobalt-Button--ghost')
        var percentDiv = th.closest('.cobalt-Stepper__Wrapper').find('.price-percent')
        var percentBlog = th.closest('.cobalt-Stepper__Wrapper').find('.cobalt-flexAlign')
        var percent_input = th.closest('.cobalt-Stepper__Wrapper').find('.percent_input')
        var min_percent_input = percent_input.attr('data-min')
        var prcie = parseInt(percentDiv.text())
        th.closest('.cobalt-Stepper__Wrapper').find('.cobalt-Stepper__ActionButton').removeClass('cobalt-Button--disabled')
        percentBlog.removeClass('danger')

        if (type == 'plus') {
            prcie += 1
        } else {
            prcie -= 1
        }

        if (min_percent_input > prcie) {
            th.addClass('cobalt-Button--disabled')
            percentBlog.addClass('danger')
            return false
        }

        var suggestprice = parseInt(suggestButton.find('.suggest-price').text())
        if (suggestprice == prcie) {
            suggestButton.addClass('hide')
        } else {
            suggestButton.removeClass('hide')
        }

        percent_input.val(prcie)

        percentDiv.text(prcie)
    })
    front('.flex_row_center').find('.cobalt-Stepper__ActionButton').trigger('click')
    front('.cobalt-Button--standalone-price').click(function (e) {
        e.preventDefault()
        var th = front(this)
        var suggprice = parseInt(th.find('.suggest-price').text())
        th.closest('.flex_row_center').find('.price-percent').text(suggprice)
        var percent_input = th.closest('.flex_row_center').find('.percent_input')
        percent_input.val(suggprice)
        th.addClass('hide')
    })

    function changeTitleBrandModel() {
        var titleModal = front('#title-modal-brand')
        if (titleModal.length) {
            var slBrand = front(".brand-model option:selected").text();
            var slModal = front(".model-import option:selected").text();
            titleModal.find('.js_title_preview').text(slBrand + ' ' + slModal)
            titleModal.removeClass('hide')
        }
    }

    jQuery('.brand-model').change()

    jQuery('#calculation-form').find('select,input').change(function (e) {
        let dataForm = jQuery('#calculation-form').serialize()
        front('#calculation-form').removeClass('not_redirect')
        front.post('/car/calculate-price/', dataForm, function (data) {
            if (data.success == true) {
                front('.price-calculated').html(data.price)
                front('#calculation-form').addClass('not_redirect')
            } else {
                front('.price-calculated').html('')
            }
        })
    });

    jQuery('.calc-button ').click(function (e) {
        let dataForm = jQuery('#calculation-form').serialize()
        front.post('/car/choose-rent-type/', dataForm, function (data) {
            if (data.success == true) {
                front('.calculation-dest').html(data.html)
            } else {
                front('.calculation-dest').html('')
            }

            front(document).on('click', '.cobalt-Button--fullWidth', function (e) {
                front('.drivy_open').val(front(this).attr('data-set'))
            });
        })

    });


    front(document).on('click', '.cobalt-Icon--colorGraphiteLight', function () {
        let csrftoken = front('input[name="csrfmiddlewaretoken"]').val();
        front.post('/variable-modal/', {
            'key': front(this).attr('data-val'),
            'class': 'monthly_earnings',
            csrfmiddlewaretoken: csrftoken
        }, function (data) {
            if (data.success == true && data.html) {
                front('.blog-modal').html(data.html)
                front("#exampleModal").modal('show')
            }
        })
    })

    front('.add-time-slot').click(function (e) {
        e.preventDefault()
        // var sec = front(this).closest('.group-section-weeks').find('.second-group-time_slot')
        // if (sec.hasClass('hide')) {
        //     sec.removeClass('hide')
        //     front(this).text(gettext('Delete time slot'))
        // } else {
        //     sec.show("slide", {direction: "right"}, 1000);
        //     sec.addClass('hide')
        //     front(this).text(gettext('Add a time slot'))
        // }
    });
    front('.checkin-checkout-title').click(function (e) {
        e.preventDefault()
        var sec = front('.checkin-checkout')
        if (sec.hasClass('hide')) {
            sec.removeClass('hide')
        } else {
            sec.addClass('hide')
        }
    });


    front(".add-more").click(function () {
        var closesDiv = front(this).closest('.group-section-weeks')
        var slots = closesDiv.find(".copy").find('.slot-clone');
        var copy = closesDiv.find(".copy")
        slots.each(function () {
            var attr = front(this).attr('name')
            var words = attr.split("_")
            var newName = words[0] + '_' + words[1] + '_' + (parseInt(words[2]) + 1) + '_' + words[3]
            front(this).attr('name', newName)
        })

        var html = copy.html();
        var sec = closesDiv.find('.second-group-time_slot')
        sec.after(html);

    });


    front('.select-type-slot').each(function (e) {
        front(this).change(function (e) {
            var slot = front(this).closest('.group-section-weeks').find('.slot-time')
            var addtimeslot = front(this).closest('.group-section-weeks').find('.add-time-slot')
            var controlgroup = front(this).closest('.group-section-weeks').find('.control-group')
            if (front(this).val() != 'custom') {
                slot.addClass('hide')
                addtimeslot.addClass('hide')
                controlgroup.addClass('hide')
            } else {
                slot.removeClass('hide')
                controlgroup.removeClass('hide')
                addtimeslot.removeClass('hide')
            }
        })
    })
    front('.select-type-slot').change()


    front("body").on("click", ".remove-slot", function () {
        front(this).closest(".control-group").remove();
    });

    front(document).on('click', '.content-calendar td', function (e) {
        e.preventDefault()
        var thisa = front(this)
        if (thisa.hasClass('noday') || thisa.hasClass('disabled-calendar')) {
            return false
        }
        var val = thisa.text()
        var month = thisa.closest('table').find('.month').text()
        var active = thisa.closest('.content-calendar').find('td.start-td')
        if (!active.length) {
            thisa.addClass('start-td')
        } else {
            thisa.addClass('end-td')
        }
    })

    front(document).on('click', '.js_show_more_months', function () {
        let val = front('.year_month_day').attr('data-last')
        let csrftoken = front('input[name="csrfmiddlewaretoken"]').val();

        front.ajax({
            method: "post",
            url: '/car/add-calendar/',
            dataType: "json",
            data: {year_month_day: val, csrfmiddlewaretoken: csrftoken},
            success: function (data) {
                if (data.success == true) {
                    front('.owner_calendar_wrapper').append(data.html)
                    front('.year_month_day').attr('data-last', data.nextmonth)
                    calendar()
                }
            }
        }), !1
    })

    function calendar() {
        if (front('.js_car_calendar ').length) {
            var i = 0
            front('.owner_calendar_period').each(function () {
                if (!front(this).hasClass('disabled')) {
                    front(this).attr('data-index', i)
                    i += 1
                }
            })
        }
    }

    calendar()

    front(document).on("submit", "#car_wizards-form", function (e) {
        e.preventDefault()

        var form = front(this);
        var url = front(this).attr('action');
        e.preventDefault();


        var fileinput = front('.file-input-d').attr('id')
        var files = front("#" + fileinput).data('fileinput').fileManager.stack

        if (!front("#" + fileinput)[0].files.length) {
            // alertifyError(gettext('Please choose image files'))
            // return false
        }

        var formData = new FormData(this);

        jQuery.each(files, function (i, file) {
            formData.append('file-' + i, files[i]['file']);
        });

        front.ajax({
            method: "post",
            url: url,
            dataType: "json",
            data: formData,
            mimeTypes: "multipart/form-data",
            contentType: false,
            cache: false,
            processData: false,
            success: function (data) {

                if (data.success == false) {
                    front.each(data.errors, function (index, value) {
                        front.each(value, function (index, value) {
                            var txt = '<li class="label-field">' + index + '<ul class="errorlist">';

                            front.each(value, function (index2, value2) {
                                txt += '<li>' + value2 + '</li>';
                            });
                            txt += '</ul></li>'
                            alertifyTogether(txt, "danger")
                        });
                    });
                } else if (data.success == true) {
                    // front('#car_wizards-form').trigger("reset");
                    // front("#"+fileinput).fileinput('reset')
                    window.location.href = data.redirect
                }

            }
        }), !1

    })

    jQuery('#calculation-form').submit(function (e) {
        if (front(this).hasClass('not_redirect')) {
            e.preventDefault()
        }
    });

    function afterAjax() {
        front(document).on('submit', '.cobalt-Button--fullWidth', function (e) {
            front('.drivy_open').val(front(this).attr('data-set'))
        });
    }

    afterAjax()

    function alertifySuccess(message, modal = null, delay = 10) {
        var msg = alertify.success('Default message');
        msg.delay(10).setContent(message);

        if (modal) {
            front('#' + modal).modal('hide');
        }
    }

    function alertifyError(message, modal = null, delay = 10) {
        var msg = alertify.warning('Default message');
        msg.delay(10).setContent(message);

        if (modal) {
            front('#' + modal).modal('hide');
        }
    }

    var dateNow = new Date()
    var endDate = new Date(dateNow.setMonth(dateNow.getMonth() + 1));

    if (front('#calendar-booking').length) {
        front('#calendar-booking').daterangepicker({
            opens: 'right',
            showDropdowns: true,
            "showWeekNumbers": true,
            "showISOWeekNumbers": true,
            "timePicker": true,
            "timePicker24Hour": true,
            "autoApply": true,
            "minDate": new Date(),
            "maxDate": endDate,
            onClose: function (dateText) {
                front(this).trigger('blur');
            }
        }, function (start, end, label) {
            console.log("A new date selection was made: " + start.format('YYYY-MM-DD') + ' to ' + end.format('YYYY-MM-DD'));
        });
    }

    var dateNow = new Date()
    var endDate = new Date(dateNow.setMonth(dateNow.getMonth() + 1));

    if (front('#start-date').length) {
        var datamax = front('#end-date').attr('data-max')
        front('#start-date').daterangepicker({
            singleDatePicker: true,
            showDropdowns: true,
            "minDate": new Date(),
            "maxDate": datamax,
            maxYear: parseInt(moment().format('YYYY'), 10)
        }, function (start, end, label) {
        });
    }

    if (front('#end-date').length) {
        var datamax = front('#end-date').attr('data-max')
        var datamin = front('#start-date').attr('data-min')
        front('#end-date').daterangepicker({
            singleDatePicker: true,
            "maxDate": datamax,
            "minDate": datamin,
            showDropdowns: true,
        }, function (start, end, label) {
        });
    }

    if (front('.rental_duration').length) {
        var from = front(".rental_duration").attr('from')
        var to = front(".rental_duration").attr('to')

        front(".rental_duration").ionRangeSlider({
            type: "double",
            min: 0,
            max: 30,
            from: from ? from : 0,
            to: to ? to : 30,
            grid: true,
            onChange: function (data) {
                if (data.from != 0) {
                    front('.rental_duration_no_minimum').text(gettext('No rentals of ' + data.from + ' hours or less\n'))
                } else {
                    front('.rental_duration_no_minimum').text(gettext('No minimum'))
                }
            },
            onFinish: function (data) {
                front('.rental_duration_maximum_days').text(gettext('Maximum ' + data.to + ' days'))
            },
        });
    }
    if (front('.maximum_booking_notice').length) {
        var custom_values2 = ['The next 15 days', 'The next month', 'The next 3 months', 'The next 6 months', 'All future dates']
        var from = front(".maximum_booking_notice").attr('to')
        front(".maximum_booking_notice").ionRangeSlider({
            grid: true,
            from: from ? custom_values2.indexOf(from) : 4,
            values: custom_values2,
        });
    }

    if (front('[data-toggle="tooltip"]').length) {
        front('[data-toggle="tooltip"]').tooltip()
    }

    function formatState(state) {
        if (!state.id) {
            return state.text;
        }
        var $state = $(
            '<span><img src="/media/flags/1x1/' + state.element.value.toLowerCase() +
            '.svg" class="flag-country-small" /> ' +
            state.text + '</span>'
        );
        return $state;
    };

    if ($(".country-list").length) {
        $(".country-list").select2({
            templateResult: formatState
        });
    }

});


function alertifyTogether(message, className, delay = 20) {
    if (className == 'success' || className == 'info') {
        var msg = alertify.success('Default message');
        msg.delay(10).setContent(message);
    } else {
        var msg = alertify.warning('Default message');
        msg.delay(10).setContent(message);
    }

}