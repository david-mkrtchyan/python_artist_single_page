$(document).ready(function() {
    //according for menu
      $( ".popular-communities-items a" ).click(function() {
          var according =  $(this).attr('aria-expanded');
           if(according === 'false') {
               $('.popular-communities-items a img').css('transform', 'rotate(0deg)');
               $(this).find('img').css('transform', 'rotate(90deg)');
           } else {
               $('.popular-communities-items a img').css('transform', 'rotate(0deg)');
               $(this).css('transform', 'rotate(0deg)');
           }
    });
    $(".register-right .joined-btn").click(function() {
        $(this).toggleClass('joined-btn-click');
    });
    $(".small-toggle-menu").click(function() {
        $('#topicsNavbar').toggleClass('open-menu-form');
    });
    $(".keep-inside-clicks-open").click(function(e){
        e.stopPropagation();
    });
    $("#toggleInput").click(function(e){
        $('body').toggleClass('activeContent');

        var classActive = $('body').hasClass('activeContent') ? true : false;

        $.ajax({
                    url: "/night-mode/",
                    type: "GET",
                    data: {
                        'night_mode' : classActive
                    },
                    processData: true,
                    contentType: false,
                    cache: false,
                    success: function (text) {
                        console.log(text)
                    },
                    error: function (text) {
                    }
                });

    });
    $(".downvoted-close").click(function(e){
        $(this).parent().css('display', 'none');
    });
});