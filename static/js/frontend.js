$(document).ready(function () {

    /*-------------------------------------
     Contact Form initiating
     -------------------------------------*/
    var registerForm = $('#register_form');
    if (registerForm.length) {
        registerForm.validator().on('submit', function (e) {
            var $this = $(this), $target = registerForm.find('.form-response');
            if (e.isDefaultPrevented()) {
                $target.html("");
            } else {
                e.preventDefault();
                var formData = new FormData(this);
                $.ajax({
                    url: "/user/register/",
                    type: "POST",
                    data: formData,
                    processData: false,
                    contentType: false,
                    cache: false,
                    success: function (text) {
                        if (text && text.success == true) {
                            registerForm.trigger("reset");
                            $('#verifiedModal').modal('hide')
                            $('#finishModal').modal('show')
                        }
                    },
                    error: function (text) {
                        if (text && text.responseText) {
                            showErrors(text.responseText, registerForm)
                        }
                    }
                });
                return false;
            }
        });
    }

    var loginModal = $('#login_form');
    if (loginModal.length) {
        loginModal.validator().on('submit', function (e) {
            var $this = $(this), $target = loginModal.find('.form-response');
            if (e.isDefaultPrevented()) {
                $target.html("");
            } else {
                e.preventDefault();
                var formData = new FormData(this);
                $.ajax({
                    url: "/user/login/",
                    type: "POST",
                    data: formData,
                    processData: false,
                    contentType: false,
                    cache: false,
                    success: function (text) {
                        if (text && text.success == true) {
                            location.href = '/'
                        }
                    },
                    error: function (text) {
                        if (text && text.responseText) {
                            showErrors(text.responseText, loginModal)
                        }
                    }
                });
                return false;
            }
        });
    }


    var reset_password = $('#reset_password');
    if (reset_password.length) {
        reset_password.validator().on('submit', function (e) {
            var $this = $(this), $target = reset_password.find('.form-response');
            if (e.isDefaultPrevented()) {
                $target.html("");
            } else {
                e.preventDefault();
                var formData = new FormData(this);
                $.ajax({
                    url: "/user/reset-password/",
                    type: "POST",
                    data: formData,
                    processData: false,
                    contentType: false,
                    cache: false,
                    success: function (text) {
                        if (text && text.success == true) {
                            location.href = '/'
                        }
                    },
                    error: function (text) {
                        if (text && text.responseText) {
                            showErrors(text.responseText, reset_password)
                        }
                    }
                });
                return false;
            }
        });
    }


    var update_email_form = $('#update_email_form');
    if (update_email_form.length) {
        update_email_form.validator().on('submit', function (e) {
            var $this = $(this), $target = update_email_form.find('.form-response');
            if (e.isDefaultPrevented()) {
                $target.html("");
            } else {
                e.preventDefault();
                var formData = new FormData(this);
                $.ajax({
                    url: "/user/update-email/",
                    type: "POST",
                    data: formData,
                    processData: false,
                    contentType: false,
                    cache: false,
                    success: function (text) {
                        if (text && text.success == true) {
                            var emailChange = $('#changeEmail')
                            emailChange.find('.login-title').text('Check your email\n')
                            emailChange.find('.success-message').html(text.message)
                            emailChange.find('.success-message').addClass('login-form')
                            update_email_form.remove()
                        }
                    },
                    error: function (text) {
                        if (text && text.responseText) {
                            showErrors(text.responseText, update_email_form)
                        }
                    }
                });
                return false;
            }
        });
    }


    var resetUsername = $('#resetUsername');
    if (resetUsername.length) {
        resetUsername.validator().on('submit', function (e) {
            var $this = $(this), $target = resetUsername.find('.form-response');
            if (e.isDefaultPrevented()) {
                $target.html("");
            } else {
                e.preventDefault();
                var formData = new FormData(this);
                $.ajax({
                    url: "/user/reset-username/",
                    type: "POST",
                    data: formData,
                    processData: false,
                    contentType: false,
                    cache: false,
                    success: function (text) {
                        if (text && text.success == true) {
                            location.href = '/user/account/'
                        }
                    },
                    error: function (text) {
                        if (text && text.responseText) {
                            showErrors(text.responseText, resetUsername)
                        }
                    }
                });
                return false;
            }
        });
    }

    function readURL(input, classChangedBlog, image = true) {
        let filechange = $('.' + classChangedBlog)
        if (filechange.length)
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    if (image) {
                        filechange.attr('src', e.target.result);
                    } else {
                        filechange.css('background-image', 'url(' + e.target.result + ')');
                    }
                }

                reader.readAsDataURL(input.files[0]);
            }
    }

    $("#file-input-4").change(function () {
        readURL(this, 'image-avatar');

        var formData = new FormData();
        formData.append('file_avatar', $('#file-input-4')[0].files[0]);

        sendAjax("/user/switch-field/", formData, "POST", false)
    });

    $("#file-input-5").change(function () {

        readURL(this, 'banner-image', false);
        var formData = new FormData();
        formData.append('file_banner', $('#file-input-5')[0].files[0]);

        sendAjax("/user/switch-field/", formData, "POST", false)

    });


    var education;
    $(document).on('change', "#uploadEducation", function () {
        var formData = new FormData();
        formData.append('file_education', $('#uploadEducation')[0].files[0]);
        education = formData
    });

    var autocomplete_change_multiple = $('.autocomplete_change_multiple');
    if (autocomplete_change_multiple.length) {
        autocomplete_change_multiple.blur(function () {
            var val = $(this).val();

            var name = $(this).attr('name')
            var dict = {}
            dict['name'] = name
            dict[name] = val
            dict['csrfmiddlewaretoken'] = $("input[name=csrfmiddlewaretoken]").val()
            sendAjax("/user/switch-field/", dict, "GET")
        });

    }

    var autocomplete_change = $('.autocomplete_change');
    if (autocomplete_change.length) {
        autocomplete_change.blur(function () {
            var val = $(this).val();

            var name = $(this).attr('name')
            var dict = {}
            dict['name'] = name
            dict[name] = val
            dict['csrfmiddlewaretoken'] = $("input[name=csrfmiddlewaretoken]").val()
            sendAjax("/user/switch-field/", dict, "GET")
        });

    }


    var autocomplete_change = $('.autocomplete_change');
    if (autocomplete_change.length) {
        autocomplete_change.blur(function () {
            var val = $(this).val();

            var name = $(this).attr('name')
            var dict = {}
            dict['name'] = name
            dict[name] = val
            dict['csrfmiddlewaretoken'] = $("input[name=csrfmiddlewaretoken]").val()
            sendAjax("/user/switch-field/", dict, "GET")
        });

    }

    function sendAjax(url, data, method = 'POST', processData = true) {
        $.ajax({
            url: url,
            type: method,
            dataType: "json",
            data: data,
            mimeTypes: "multipart/form-data",
            contentType: false,
            cache: false,
            processData: processData,
            success: function (text) {
            },
            error: function (text) {
                if (text && text.responseText) {
                    showErrors(text.responseText, changePassword_settings)
                }
            }
        });
    }

    var switch_details = $('.settings-switch');
    if (switch_details.length) {
        $(document).on('click', '.settings-switch .switch', function (e) {
            var checked = $(this).find("input[type=checkbox]:checked").length > 0;
            var name = $(this).find("input[type=checkbox]").attr('name')
            var dict = {}
            dict['name'] = name
            dict[name] = checked
            dict['csrfmiddlewaretoken'] = $("input[name=csrfmiddlewaretoken]").val()
            sendAjax("/user/switch-field/", dict, "GET")
        })
    }
    var settings_switch_select = $('.settings-switch_select');
    if (settings_switch_select.length) {
        settings_switch_select.find('select').change(function (e) {
            var checked = $(this).val()
            var name = $(this).attr('name')
            var dict = {}
            dict['name'] = name
            dict[name] = checked
            dict['csrfmiddlewaretoken'] = $("input[name=csrfmiddlewaretoken]").val()
            sendAjax("/user/switch-field/", dict, "GET")
        })
    }
    var drop_down_link = $('.drop_down_link');
    if (drop_down_link.length) {
        drop_down_link.find('a').click(function (e) {
            var val = $(this).attr('data-name')
            var name = $(this).closest('.drop_down_link').attr('name')
            var dict = {}
            dict['name'] = name
            dict[name] = val
            dict['csrfmiddlewaretoken'] = $("input[name=csrfmiddlewaretoken]").val()
            sendAjax("/user/switch-field/", dict, "GET")
            $(this).closest('.settings-absolute').find('.change_comm_sort').text(val)
            if ($(this).find('img').length) {
                var button = $(this).closest('.settings-absolute').find('button')
                button.find('img').remove()
                var p_prime = $(this).find('img').clone();
                button.prepend(p_prime)
            }
        })
    }

    var changePassword_settings = $('#update_password_form');
    if (changePassword_settings.length) {
        changePassword_settings.validator().on('submit', function (e) {
            var $this = $(this), $target = changePassword_settings.find('.form-response');
            if (e.isDefaultPrevented()) {
                $target.html("");
            } else {
                e.preventDefault();
                var formData = new FormData(this);
                $.ajax({
                    url: "/user/change-password/",
                    type: "POST",
                    data: formData,
                    processData: false,
                    contentType: false,
                    cache: false,
                    success: function (text) {
                        if (text && text.success == true) {
                            location.href = '/user/account/'
                        }
                    },
                    error: function (text) {
                        if (text && text.responseText) {
                            showErrors(text.responseText, changePassword_settings)
                        }
                    }
                });
                return false;
            }
        });
    }


    function showErrors(jsondata, objectForm) {
        var obj = jQuery.parseJSON(jsondata);
        $('.help-block.with-errors').html('');
        $.each(obj, function (k, v) {
            var input = objectForm.find("input[name*='" + k + "']");
            var textarea = objectForm.find("textarea[name*='" + k + "']");
            var label = '';

            if (input.length) {
                var groupParent = input.closest('.form-group')
                var label = input.attr('data-label')
            }

            if (textarea.length) {
                var groupParent = textarea.closest('.form-group')
                var label = input.attr('data-label')
            }

            if (label) {
                var txt = '<li class="label-field">' + label + '<ul class="errorlist">';
                $.each(v, function (s, n) {
                    txt += '<li>' + n + '</li>';
                })
                txt += '</ul></li>';
                alertifyTogether(txt, "danger")
            } else {
                var txt = '<li class="label-field"><ul class="errorlist">';
                $.each(v, function (s, n) {
                    txt += '<li>' + n + '</li>';
                })
                txt += '</ul></li>';
                alertifyTogether(txt, "danger")
            }
        });
    }


    $(document).ready(function () {
        function readURL(input, classname = 'file-change') {
            let filechange = $('.' + classname)
            if (filechange.length)
                if (input.files && input.files[0]) {
                    var reader = new FileReader();

                    reader.onload = function (e) {
                        filechange.attr('src', e.target.result);
                    }

                    reader.readAsDataURL(input.files[0]);
                }
        }

        $("#uploadDoc").change(function () {
            readURL(this);
        });

        $("#uploadDoc2").change(function () {
            readURL(this, 'file-change2');
        });
    })

});


function alertifyTogether(message, className, delay = 20) {
    if (className == 'success' || className == 'info') {
        var msg = alertify.success('Default message');
        msg.delay(10).setContent(message);
    } else {
        var msg = alertify.warning('Default message');
        msg.delay(10).setContent(message);
    }

}