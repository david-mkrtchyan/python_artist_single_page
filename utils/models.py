from django.db import models
from django_enumfield import enum
# Create your models here.
from enum import Enum
from PIL import Image
import secrets
import os
from django.utils import timezone
from artist.settings import BASE_DIR
from utils.functions import dir_exists
# Create your models here.


type_variable = 1
type_artwork = 2
type_gallery = 3

class ImageType(enum.Enum):
    variable = type_variable
    artwork = type_artwork
    gallery = type_gallery

class ImagesTrait(object):

    @property
    def getImages(self):
        return Images.objects.filter(type=self.imageType, object_id=self.id).order_by('-rank').all()

    @property
    def getImage(self):
        return Images.objects.filter(type=self.imageType, object_id=self.id).order_by('-rank').first()

    @property
    def getImagePath(self, size='big'):
        objImage = self.getImage
        if objImage:
            return Images.get_path(objImage, size)


class Artworks(models.Model, ImagesTrait):
    imageType = type_artwork
    name = models.CharField(max_length=255, null=False, blank=False)
    gallery_content_name = models.CharField(max_length=255, null=False, blank=False, default='Lorem' )
    description = models.TextField(null=False, blank=False, default='Lorem')

    def __str__(self):
        return self.name

    @property
    def getGalleries(self):
        return Gallery.objects.filter(category_id=self.id).all()

    class Meta:
        db_table = 'artworks'


class Gallery(models.Model, ImagesTrait):
    imageType = type_gallery
    name = models.CharField(max_length=255, null=False, blank=False)
    year = models.IntegerField(null=True, blank=True)
    content = models.TextField(null=False, blank=False)
    category = models.ForeignKey(Artworks, on_delete=models.CASCADE)

    def __str__(self):
        return self.name

    class Meta:
        db_table = 'galleries'


class Contact(models.Model):
    email = models.CharField(max_length=255, null=False, blank=False)
    phone_number = models.CharField(max_length=255, null=False, blank=False)
    name = models.CharField(max_length=255, null=False, blank=False)
    message = models.TextField(max_length=255, null=True, blank=True)
    original_work_by_artist = models.BooleanField(default=True)
    limited_edition_prints_sculpture = models.BooleanField(default=True)
    newsletter_from_artist = models.BooleanField(default=True)

    class Meta:
        db_table = 'contacts'


class VariableType(Enum):
    number = 1
    text = 2
    textArea = 3
    image = 4
    select = 5


class Variables(models.Model):
    name = models.CharField(max_length=255, null=False, blank=False)
    value = models.TextField(null=True, blank=True)
    type = models.SmallIntegerField(default=VariableType.number)
    key = models.SlugField(null=False, blank=False, unique=True)
    multiple = models.BooleanField(default=False)
    choice_values = models.TextField(null=True, blank=True, )

    @property
    def getTypeName(self):
        return '\n'.join(str(i.name) for i in VariableType if i.value == self.type)

    @staticmethod
    def byKey(key):
        return Variables.objects.filter(key=key).first()

    def __str__(self):
        return str(self.name)


class Images(models.Model):
    path = models.ImageField()
    object_id = models.IntegerField()
    type = models.SmallIntegerField(default=ImageType.variable)
    created_at = models.DateField(auto_now_add=True)
    updated_at = models.DateField(timezone.now, default=timezone.now)
    rank = models.IntegerField(default=0, null=True)

    def save(self, *args, **kwargs):

        if not self.path:
            super().save(*args, **kwargs)

        elif self.path:
            img = Image.open(self.path.path)

            random_hex = secrets.token_hex(8)
            _, f_ext = os.path.splitext(img.filename)
            picture_fn = random_hex + f_ext

            bigsize = f'{BASE_DIR}/media/uploads/big/{self.type}/'
            dir_exists(bigsize)

            picture_path = os.path.join(bigsize, picture_fn)
            img.save(picture_path)
            if os.path.isfile(self.path.path):
                os.remove(self.path.path)
                self.path = f'{picture_fn}'
                super().save(*args, **kwargs)

            if img.width > 465 or img.height > 300:
                smallsize = f'{BASE_DIR}/media/uploads/small/{self.type}/'
                dir_exists(smallsize)
                picture_path = os.path.join(smallsize, picture_fn)
                output_size = (465, 300)
                img.resize(output_size, Image.ANTIALIAS)
                img.save(picture_path, quality=100)

    @staticmethod
    def get_path(obj, size='big'):
        return f'/media/uploads/{size}/{obj.type}/{obj.path}'

    class Meta:
        db_table = 'images'
