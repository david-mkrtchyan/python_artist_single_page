import os
import datetime
import random
import string

def handle_uploaded_file(f, dirname=None):
    import os
    import secrets
    from artist.settings import MEDIA_ROOT, STATIC_ROOT, BASE_DIR

    _, f_ext = os.path.splitext(f.name)
    originFile = None
    try:
        if not dirname:
            dirname = BASE_DIR + '/media/uploads/'

        os.makedirs(dirname, mode=0o777, exist_ok=True)

        random_hex = secrets.token_hex(8)
        file_name = random_hex + f_ext
        originFile = f'{dirname}{file_name}'
        with open(originFile, 'wb+') as destination:
            for chunk in f.chunks():
                destination.write(chunk)
    except:
        pass

    return originFile


def dir_exists(directory):
    if not os.path.isdir(directory):
        os.makedirs(directory)


def file_exists(file):
    if os.path.isfile(file):
        return True
    return False

def getMonthAgo(month):
    return datetime.date.today() - datetime.timedelta(int(month)*365/12)

def uppercaseWordSeperateToOneString(str):
    return ''.join(' ' + x if x.isupper() else x for x in str).strip(' ')

def set_cookie(response, key, value, days_expire = 7):
  if days_expire is None:
    max_age = 365 * 24 * 60 * 60  #one year
  else:
    max_age = days_expire * 24 * 60 * 60
  expires = datetime.datetime.strftime(datetime.datetime.utcnow() + datetime.timedelta(seconds=max_age), "%a, %d-%b-%Y %H:%M:%S GMT")
  response.set_cookie(key, value, max_age=max_age, expires=expires)


def randomString(stringLength=10):
    """Generate a random string of fixed length """
    letters = string.ascii_lowercase
    return ''.join(random.choice(letters) for i in range(stringLength))


