from . import views
from django.urls import path, include
from rest_framework_simplejwt import views as jwt_views

urlpatterns = [
    path('gallery/<int:id>', views.GalleryApi.as_view()),
    path('contact/', views.Contact.as_view()),
    path('seed/', views.seed),
    path('remove-image/<int:id>', views.removeImage, name='remove-image'),
    path('sort-images', views.imageSort, name='sort-images'),
    path('image-delete', views.imageDelete, name='image-delete'),
]
