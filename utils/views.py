from django.shortcuts import render, get_object_or_404
from django.http import JsonResponse
from django.contrib.auth.decorators import user_passes_test
from django.contrib.auth.decorators import login_required
from utils.models import Images, Gallery, Artworks
import json
from django.views.decorators.csrf import csrf_exempt
from utils.seeds import Seeds
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status
from .serializers import ContactSerializer
from artist.settings import SITE_URL
from utils.models import Variables
# Create your views here.

@user_passes_test(lambda u: u.is_superuser)
def seed(request):
    Seeds.saveDb()

    return JsonResponse({'imported': True})

class Contact(APIView):

    def post(self, request, *args, **kwargs):
        serializer = ContactSerializer(data=request.data)
        if serializer.is_valid(raise_exception=True):
            serializer.create(serializer.validated_data)
            return Response({'success': True, 'message': 'Your contact have been sent successfully'},
                            status=status.HTTP_200_OK)


class GalleryApi(APIView):

    def get(self, request, *args, **kwargs):

        data = {}

        artwork = Artworks.objects.filter(id=kwargs.get('id')).first()

        data['galleries'] = [{'name': k.name, 'content': k.content, 'year': k.year, 'category': k.category.name, 'image': SITE_URL+k.getImagePath, 'id': i} for i, k in
                            enumerate(artwork.getGalleries)]

        data['gallery_text_name'] = artwork.gallery_content_name
        data['gallery_text_desc'] = artwork.description

        return Response(
            {'success': True, 'data': data, },
            status=status.HTTP_200_OK)



@user_passes_test(lambda u: u.is_superuser)
@login_required
def removeImage(request, id):
    if request.method == 'GET':
        get_object_or_404(Images, id=id).delete()
        return JsonResponse({'sucess': True})
    return JsonResponse({'sucess': False})


@user_passes_test(lambda u: u.is_superuser)
@login_required
def imageSort(request):
    stacks = request.POST.get('stack')
    if request.method == 'POST' and stacks:
        jsonData = json.loads(stacks)
        for key, value in enumerate(jsonData):
            Images.objects.filter(id=int(value['key'])).update(rank=int(key + 1))

    return JsonResponse({'sucess': True})


@user_passes_test(lambda u: u.is_superuser)
@login_required
@csrf_exempt
def imageDelete(request):
    id = request.POST.get('key')
    if request.method == 'POST' and id:
        Images.objects.filter(id=int(id)).delete()
    return JsonResponse({'sucess': True})

