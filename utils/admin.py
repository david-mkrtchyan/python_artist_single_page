from .models import Variables, VariableType, Artworks, Contact, Gallery
from .admin_forms import VariableAdminForm,ArtworkAdminForm, ContactAdminForm, GalleryAdminForm
from django.contrib import admin
from utils.models import Images
from utils.templatetags.file_exists import file_exists
from django.urls import reverse
from utils.functions import handle_uploaded_file
# Register your models here.
from utils.models import ImageType
from django.utils.safestring import mark_safe

class VariableAdmin(admin.ModelAdmin):

    def value(self):
        if self.type == VariableType.image.value:
            imagesObject = Images.objects.filter(object_id=self.pk, type=ImageType.variable).order_by('rank')
            images = ''
            if imagesObject:
                for image in imagesObject:
                    if(str(file_exists(image.get_path(image)))):
                        images +=  '<img src="'+image.get_path(image)+'" class="img-variable">'
            return mark_safe(images)
        return  mark_safe(self.value)

    list_display = ('name',value , )

    fields = ['name', 'value']

    form = VariableAdminForm

    change_form_template = 'admin/variables/change_form.html'

    def __init__(self, *args, **kwargs):
        super(VariableAdmin, self).__init__(*args, **kwargs)
        # self.list_display_links = None

    def save_model(self, request, obj, form, change):

        if int(obj.type) == int(VariableType.select.value):
            import ast
            if obj.multiple == True:
                l = ast.literal_eval(obj.value)
                obj.value = ','.join(l)

        elif int(obj.type) == int(VariableType.image.value):

            images = request.FILES.getlist('images')

            if images:
                for image in images:
                    writenFile = handle_uploaded_file(image)
                    if writenFile:
                        if obj.getImage:
                            obj.getImage.delete()

                        Images.objects.create(
                            object_id=obj.id,
                            type=ImageType.variable,
                            path=writenFile,
                        )

        obj.save()

        super(VariableAdmin, self).save_model(request, obj, form, change)

    def change_view(self, request, object_id, form_url='', extra_context=None):

        extra_context = extra_context or {}

        imagesObject = Images.objects.filter(object_id=object_id, type=ImageType.variable).order_by('rank')
        images = imagesConfig = ''

        if imagesObject:
            for image in imagesObject:
                images += '"' + str(file_exists(image.get_path(image))) + '",'
                imagesConfig += "{'url': '" + reverse('image-delete') + "', 'key':'" + str(
                    image.id) + "', 'extra' : {'key':'" + str(image.id) + "', } },"

        extra_context['images'] = str(images)
        extra_context['imagesConfig'] = imagesConfig

        return super().change_view(
            request, object_id, form_url, extra_context=extra_context,
        )

    def has_delete_permission(self, request, obj=None):
        return False

    def has_add_permission(self, request):
        return False

    def get_actions(self, request):
        actions = super(VariableAdmin, self).get_actions(request)
        if 'delete_selected' in actions:
            del actions['delete_selected']
        return actions



class ContactAdmin(admin.ModelAdmin):
    list_display = ('name', 'email' , 'phone_number' )
    form = ContactAdminForm


class GalleryAdmin(admin.ModelAdmin):

    def image(self):
        imagesObject = Images.objects.filter(object_id=self.pk, type=ImageType.gallery).order_by('rank')
        images = ''
        if imagesObject:
            for image in imagesObject:
                if (str(file_exists(image.get_path(image)))):
                    images += '<img src="' + image.get_path(image) + '" class="img-variable">'
        return mark_safe(images)

    list_display = (image, 'name' , 'category', 'year')
    form = GalleryAdminForm

    change_form_template = 'admin/gallery/change_form_gallery.html'


    def save_model(self, request, obj, form, change):
        obj.save()

        images = request.FILES.getlist('image')

        if images:
            for image in images:
                writenFile = handle_uploaded_file(image)
                if writenFile:
                    if obj.getImage:
                        obj.getImage.delete()

                    Images.objects.create(
                        object_id=obj.id,
                        type=ImageType.gallery,
                        path=writenFile,
                    )


        super(GalleryAdmin, self).save_model(request, obj, form, change)



class ArtworkAdmin(admin.ModelAdmin):

    def value(self):
        imagesObject = Images.objects.filter(object_id=self.pk, type=ImageType.artwork).order_by('rank')
        images = ''
        if imagesObject:
            for image in imagesObject:
                if(str(file_exists(image.get_path(image)))):
                    images +=  '<img src="'+image.get_path(image)+'" class="img-variable">'
        return mark_safe(images)

    list_display = ('name', value , )

    form = ArtworkAdminForm

    change_form_template = 'admin/utils/change_form_artwork.html'

    def __init__(self, *args, **kwargs):
        super(ArtworkAdmin, self).__init__(*args, **kwargs)

    def save_model(self, request, obj, form, change):

        obj.save()
        images = request.FILES.getlist('images')

        if images:
            for image in images:
                writenFile = handle_uploaded_file(image)
                if writenFile:
                    if obj.getImage:
                        obj.getImage.delete()

                    Images.objects.create(
                        object_id=obj.id,
                        type=ImageType.artwork,
                        path=writenFile,
                    )


        super(ArtworkAdmin, self).save_model(request, obj, form, change)

    def change_view(self, request, object_id, form_url='', extra_context=None):

        extra_context = extra_context or {}

        imagesObject = Images.objects.filter(object_id=object_id, type=ImageType.artwork).order_by('rank')
        images = imagesConfig = ''

        if imagesObject:
            for image in imagesObject:
                images += '"' + str(file_exists(image.get_path(image))) + '",'
                imagesConfig += "{'url': '" + reverse('image-delete') + "', 'key':'" + str(
                    image.id) + "', 'extra' : {'key':'" + str(image.id) + "', } },"

        extra_context['images'] = str(images)
        extra_context['imagesConfig'] = imagesConfig

        return super().change_view(
            request, object_id, form_url, extra_context=extra_context,
        )


admin.site.register(Variables, VariableAdmin)
admin.site.register(Artworks, ArtworkAdmin)
admin.site.register(Gallery, GalleryAdmin)
admin.site.register(Contact, ContactAdmin)
