from django import template
import sys
from artist.settings import MEDIA_URL, BASE_DIR
from pathlib import Path
from PIL import Image
import os

register = template.Library()


@register.filter(name='file_exists')
def file_exists(filepath):
    if os.path.exists(BASE_DIR+filepath):
        return filepath

    return MEDIA_URL + 'utils/default.jpg'

register.filter(file_exists)
