from django import forms
from .models import Variables

from ckeditor_uploader.widgets import CKEditorUploadingWidget
from ckeditor.widgets import CKEditorWidget
from utils.models import VariableType, Artworks, Contact, Gallery


class VariableAdminForm(forms.ModelForm):

    def __init__(self, *args, **kwargs):
        super(VariableAdminForm, self).__init__(*args, **kwargs)

        instance = kwargs.get('instance')
        if instance and instance.pk:
            if int(instance.type) == int(VariableType.textArea.value):
                self.fields['value'] = forms.CharField(widget=CKEditorUploadingWidget())
            elif int(instance.type) == int(VariableType.image.value):
                self.fields['value'].required = False
            elif int(instance.type) == int(VariableType.select.value):
                values = instance.choice_values.split(',')
                if instance.multiple == True:

                    selected = instance.value.split(',')

                    self.fields['value'] = forms.MultipleChoiceField(
                        choices=[(i, i) for i in values],
                        initial=(c for c in selected),
                        widget=forms.CheckboxSelectMultiple()
                    )

                    self.initial['value'] = [i for i in selected ]

                else:
                    self.fields['value'] = forms.ChoiceField(choices=[(i, i) for i in values])
                    self.initial['value'] = instance.value



    class Meta:
        model = Variables
        exclude = []

class ArtworkAdminForm(forms.ModelForm):
    image = forms.ImageField()
    description = forms.CharField(widget=CKEditorUploadingWidget())

    class Meta:
        model = Artworks
        exclude = []

class ContactAdminForm(forms.ModelForm):
    class Meta:
        model = Contact
        exclude = []


class GalleryAdminForm(forms.ModelForm):
    content = forms.CharField(widget=CKEditorUploadingWidget())
    image = forms.ImageField()

    def __init__(self, *args, **kwargs):
        super(GalleryAdminForm, self).__init__(*args, **kwargs)

        instance = kwargs.get('instance')
        if instance and instance.pk:
            if instance.getImagePath:
                self.fields['image'].required = False

    class Meta:
        model = Gallery
        exclude = []

