from users.models import Users as User
from django.utils.translation import gettext as _
from django.contrib.auth.hashers import make_password
from rest_framework import serializers
from django.db import DatabaseError, transaction
from django import forms
from datetime import date
from utils.models import Contact

class ContactSerializer(serializers.ModelSerializer):

    email = serializers.CharField(required=True)
    phone_number = serializers.CharField(required=True)
    name = serializers.CharField(required=True)
    message = serializers.CharField(required=True)
    original_work_by_artist = serializers.BooleanField(required=False)
    limited_edition_prints_sculpture = serializers.BooleanField(required=False)
    newsletter_from_artist = serializers.BooleanField(required=False)

    def create(self, validated_data):
        instance = self.Meta.model(**validated_data)
        instance.save()
        return instance

    class Meta:
        model = Contact
        fields = ('email', 'phone_number', 'name', 'message', 'original_work_by_artist', 'limited_edition_prints_sculpture', 'newsletter_from_artist')
