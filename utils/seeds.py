
from .models import Variables, VariableType


class Seeds:

    def __init__(self):
        pass

    @staticmethod
    def saveDb():

        variable = Variables.objects.filter(key='header_image').first()
        if not variable:
            Variables.objects.create(
                name='Header Image',
                value='/media/uploads/big/1/generic-bg.png',
                type=VariableType.image.value,
                key='header_image',
                multiple=False,
            )

        variable = Variables.objects.filter(key='artwork_content').first()
        if not variable:
            Variables.objects.create(
                name='Artwork Content',
                value='Robert Bissell currently splits his time between London and San Francisco. He exhibits primarily in the United States and London.',
                type=VariableType.textArea.value,
                key='artwork_content',
                multiple=False,
            )

        variable = Variables.objects.filter(key='artist_image').first()
        if not variable:
            Variables.objects.create(
                name='Artist Image',
                value='/media/uploads/big/1/item4.png',
                type=VariableType.image.value,
                key='artist_image',
                multiple=False,
            )

        variable = Variables.objects.filter(key='artist_content').first()
        if not variable:
            Variables.objects.create(
                name='Artist Content',
                value='Robert Bissell grew up on a farm in Somerset, England, where animals, Celtic legends and panoramic landscapes were part of his daily life. His keen interest in visuals began at an early age, documenting life around him through his photography practice. Bissell studied at the Manchester Art College and the Royal College of Art. in London. Bissell then spent four years traveling the world, working on cruise ships to pay his way.For the next 15 years he worked as an advertising photographer and creative director in San Francisco. After burning out in the advertising business he began to paint for himself, exploring the local landscape and introducing animals into allegorical interpretations of his imagined. Bissell harrativesas recently been painting realist landscapes in oils based on his own photography. He regularly exhibits in museums and galleries across the United States and Europe.',
                type=VariableType.textArea.value,
                key='artist_content',
                multiple=False,
            )

        variable = Variables.objects.filter(key='email_content').first()
        if not variable:
            Variables.objects.create(
                name='Email Content',
                value='Thank you for your interest in artist Robert Bissell Please provide us with the information below and authorized gallery representatives will be in touch with you shortly.',
                type=VariableType.textArea.value,
                key='email_content',
                multiple=False,
            )

        variable = Variables.objects.filter(key='ordinary_beauty').first()
        if not variable:
            Variables.objects.create(
                name='Ordinary Beauty',
                value='Bissell’s landscape paintings often illustrate the dichotomy between the natural environment and the creations of humans. ',
                type=VariableType.textArea.value,
                key='ordinary_beauty',
                multiple=False,
            )
