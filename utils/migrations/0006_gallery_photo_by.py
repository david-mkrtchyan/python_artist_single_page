# Generated by Django 3.0.6 on 2020-05-13 07:00

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('utils', '0005_gallery'),
    ]

    operations = [
        migrations.AddField(
            model_name='gallery',
            name='photo_by',
            field=models.CharField(blank=True, max_length=255, null=True),
        ),
    ]
