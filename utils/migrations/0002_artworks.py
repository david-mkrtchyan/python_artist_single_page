# Generated by Django 3.0.6 on 2020-05-08 10:47

from django.db import migrations, models
import utils.models


class Migration(migrations.Migration):

    dependencies = [
        ('utils', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Artworks',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=255)),
            ],
            options={
                'db_table': 'artworks',
            },
            bases=(models.Model, utils.models.ImagesTrait),
        ),
    ]
